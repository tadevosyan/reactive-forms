import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

declare var jquery:any;
declare var $ :any;


@Component ({ 
    selector : "form-validation",
    templateUrl : '/app.formvalidation.html',
    styles : [`
    #beforeSubmit {
        display: none;
    }
    
    `
        
    ]
    

})


export class FormValidationComponent { 
    complexForm : FormGroup;

    constructor(example: FormBuilder){
        this.complexForm = example.group({
            'firstName' :[null, Validators.compose([
                Validators.required, 
                Validators.minLength(3),
                Validators.maxLength(10) 
            ])],
            'lastName' : [null, Validators.compose([
                Validators.required, 
                Validators.minLength(3), 
                Validators.maxLength(15)
            ])],
            'gender' : [null,Validators.required],
            'hiking' :  [false, Validators.required],
            'swimming' : [false, Validators.required],
            'running' : [false, Validators.required],
        })
    }
    
    submitForm(value: any){
        console.log(value);
    }


}