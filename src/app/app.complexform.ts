import { Component } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";

@Component ({

    selector : 'complex-form',
    templateUrl : './app.complexform.html',

})

export class ComplexFormComponent { 
    complexForm : FormGroup;
    
      
      constructor(example: FormBuilder){
        
        this.complexForm = example.group({
          'firstName' : '',
          'lastName': '',
          'gender' : 'Female',
          'hiking' : true,
          'running' : false,
          'swimming' : false
        })
      }
    
      // Again we’ll implement our form submit function that will just console.log the results of our form
    submitForm(value: any):void{
        console.log('Reactive Form Data: ')
        console.log(value);
    }
}